package SMTP;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class fileHandle {
    private File f;
    private FileInputStream in;
    private FileOutputStream out;
    private String filename;
    private ArrayList<String> dataArray;

    public fileHandle(){
        filename = "tcp.log";
        //this.f = f;
        f = new File(filename);
        dataArray = new ArrayList<>();
    }
    public void write(String data) throws IOException {
        try {
                dataArray.add(data + "\r\n");
                out = new FileOutputStream(f);

                for (String d: dataArray){
                    byte[] b = d.getBytes("US-ASCII");
                    out.write(b);
                }

        }finally{
            if (out != null) out.close();
        }
    }
    public void close() throws IOException {
        out.flush();
        out.close();
    }
    public void read() throws IOException{
        in = new FileInputStream(f);
        int i= in.read();
        System.out.print((char)i);
    }

}
