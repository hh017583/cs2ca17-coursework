
package SMTP;

        import java.io.*;
        import java.net.ServerSocket;
        import java.net.Socket;

public class Server {
    private static ServerSocket ss;
    private static Socket s;
    private int PORT = 25;
    private static String IP = "127.0.0.1";
    private static String Client_name;
    private static String emailFROM;
    private static String emailTO;
    private static String emailData;


    static PrintStream ps;
    static BufferedReader br;
    private static BufferedReader in;
    private static PrintWriter pw;
    private static String emailaddress = "student-loan@gov.uk";
    private static boolean is_connected;
    private static InputStream input;
    private static String clientHost;
    private static fileHandle filehandle;


    /**
     *  Constructor-  initialnise the elements
     * @param ip
     * @param port
     * @throws IOException
     */
    public Server(String ip, int port) throws IOException {
        IP = ip;
        PORT = port;
        is_connected = false;
        ss = new ServerSocket(PORT);
        s = ss.accept();
        filehandle = new fileHandle();
        ps = new PrintStream(s.getOutputStream());
        br = new BufferedReader(new InputStreamReader(System.in));
        in = new BufferedReader(new InputStreamReader(s.getInputStream()));
        pw = new PrintWriter(s.getOutputStream(), true);
    }


    /**
     * this function encapsulates the script of sending a message.
     *
     * @param msg
     * @throws IOException
     */
    public static void send(String msg) throws IOException{
        OutputStream out = s.getOutputStream();
        OutputStreamWriter ow = new OutputStreamWriter(out);
        byte buff[] = msg.getBytes();
        ow.write(String.valueOf(buff));
        pw = new PrintWriter(out, true);
        ps.println(msg);
        filehandle.write("S:"+msg);
    }

    /**
     *  this function check the income_msg
     *  if the message starts with 250
     *      it will check the command of the message
     *          for example, if it is HELLO, declared the username
     *          if it is MAIL FROM, declare the emailFROM
     *          if it is RCPT TP, declare the emailTO
     *          if it is DATA, declare the content of the email
     *
     *
     *  if the message not starts with 250, but starts with 221
     *          reply back to client, say it closing the connection
     *          and close the conneciton.
     * @param income_msg
     * @throws Exception
     */
    public static void listen(String income_msg) throws Exception {

        if(!income_msg.startsWith("250")){
            if(income_msg.startsWith("251")){
                //every thing has sent
                filehandle.write("C:"+income_msg);
                send("251"+" ok Message accepted for delivery");
            }else if(income_msg.startsWith("221")){

                // client request disconnect
                System.out.println("C:" + income_msg);
                filehandle.write("C:" + income_msg);
                send("221 gov.uk closing connection");
                System.out.println("S:221 gov.uk closing connection");
                is_connected = false;
            }
            else{

                throw new Exception("250 reply not received from client");
            }
        }
        //else{
            String[] s_in = income_msg.split(" ");
        if (s_in.length >1){
            //System.out.println("Debug::s_in[0]="+s_in[0]+" s_in[1]="+s_in[1]+" s_in[2]="+s_in[2]);

        }

            if (s_in[1].equalsIgnoreCase("HELLO")){
                String name = s_in[2];
                Client_name = name;
                System.out.println("C:" + income_msg);
                filehandle.write("C:"+ income_msg);
                send("250 hello "+Client_name+", pleased to meet you");
                System.out.println("S:250 hello "+Client_name+", pleased to meet you");
            }
            else if (s_in[1].equalsIgnoreCase("MAIL") && (s_in[2].equalsIgnoreCase("FROM:") ||s_in[2].equalsIgnoreCase("FROM") )){
                String From = s_in[3];
                emailFROM = From;
                System.out.println("C:" + income_msg);
                filehandle.write("C:"+ income_msg);
                send("250 ok");
                System.out.println("S:250 ok");
                //System.out.println("deug::emailFROM:"+emailFROM);
            }
            else if (s_in[1].equalsIgnoreCase("RCPT") && s_in[2].equalsIgnoreCase( "TO")){
                String To = s_in[3];
                emailTO = To;
                System.out.println("C:" + income_msg);
                filehandle.write("C:"+ income_msg);
                send("250 ok");
                System.out.println("S:250 ok");
                System.out.println("deug::emailTo:"+emailTO);
            }
            else if (s_in[1].equalsIgnoreCase( "DATA")){
                System.out.println("C:"+ income_msg);
                filehandle.write("C:"+ income_msg);
                send("354 End data with <CR><LF>.<CR><LF>");
                System.out.println("S:354 End data with <CR><LF>.<CR><LF>");

                String data = in.readLine();
                emailData = data;
                if (!emailData.isEmpty()){
                    System.out.println("C:"+ data);
                    filehandle.write("C:"+ data);
                    send("250 ok - Message accepted for delivery");
                    System.out.println("S:250 - Message accepted for delivery");
                }else{
                    System.err.println("emailData error");
                }
            }


            // default
            else{

            System.out.println("C:"+ income_msg);
            filehandle.write("C:"+ income_msg);
            send("250 ok");
            System.out.println("S:250 ok");

            }
      //  }
    }

    /**
     * this fucntion send a 220 message to the port
     * who ever connected to the port and reply the message
     * this will establish the connection.
     *
     * @throws IOException
     */
    public static void three_hand_shake() throws IOException {
        //send 220
        String s = "220 gov.uk";
        System.out.println("S:"+s);
        send(s);
        //filehandle.write(s);
        // receive 220 back
        String rec = in.readLine();
        String r[] = rec.split(" ");
        clientHost = r[2];
        //System.out.println("::clientHost:"+ clientHost);
        System.out.println("C:"+rec);
        filehandle.write("C:"+rec);
        if (rec.startsWith("220")){
            // send 250
            String[] r1 = rec.split(" ");
            is_connected = true;
            String s2 = "250 Hello " + r1[2] + ", please to meet you";
            System.out.println("S:"+ s2);
            // filehandle.write("S:"+s2);
            send(s2);
        }else{
            System.err.println("three hand shake failure");
            filehandle.write("three hand shake failure");
        }
    }


    /**
     *  main fucntion of server
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        Server smtp = new Server("127.0.0.1", 25);

        if (args.length > 0){
            String cIP = args[0];
            int port = Integer.parseInt(args[1]);
            System.out.println("host:"+cIP);
            System.out.println("port:"+port);
            System.out.println();
            smtp = new Server(cIP, port);
        }

        three_hand_shake();


        System.out.println("New Client successfully connected at "+ smtp.IP+ ":"+ smtp.PORT);
        while (is_connected){

            if (in.ready()){
                String str = in.readLine();
                listen(str);


            }

        }
        filehandle.close();
        s.close();
        ss.close();
    }
}
