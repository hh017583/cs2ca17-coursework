package SMTP;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Client {
    private static ServerSocket ss;
    private static Socket s;

    private static PrintWriter output;
    private static String SERVER_IP = "127.0.0.1";
    private static int SERVER_PORT = 25;
    private static String userEmail = "student@reading.ac.uk";
    static boolean is_connected;
    private static String serverHost;
    private static String localHost = "reading.ac.uk";
    private static BufferedReader input;
    private static BufferedReader Key;

    /**
     *  constructor of ClientV2
     *  this initialise the elements of Socket, BufferedReader, PrintWriter.
     * @param ip
     * @param port
     * @throws IOException
     */
    public Client(String ip, int port) throws IOException{
       SERVER_PORT = port;
       SERVER_IP = ip;

        s = new Socket(SERVER_IP, SERVER_PORT);
        is_connected = false;
        input = new BufferedReader(new InputStreamReader(s.getInputStream()));
        Key = new BufferedReader(new InputStreamReader(System.in));
        output = new PrintWriter(s.getOutputStream(), true);
    }

    /**
     *  checking the income_msg starts with 250 or 221, otherwise throw exception.
     * @param income_msg
     * @throws Exception
     */

    public static void listen(String income_msg) throws Exception { // income msg
        if(!income_msg.startsWith("250")){
            if(income_msg.startsWith("251")){
                send(250, "ok");
                System.out.println("C:250 ok");
            }else if(income_msg.startsWith("221")){
                is_connected = false;
            }


            else{

                throw new Exception ("250 reply not received from server.");
            }
        }
    }

    /**
     *  simplify script of sending messages to server.
     * @param signal
     * @param data
     */
    public static void send(int signal, String data){
        output.println(signal +" "+ data);
    }

    /**
     *  this function do the three_hand_shake and return to if the process successfully completed
     *
     *  this will first receive the 220 message from the server, than reply 220 for request connecton
     *  if it received the response from server, the connection is established.
     * @param input
     * @return
     * @throws IOException
     */
    public static boolean three_hand_shake(BufferedReader input) throws IOException {
        //rece 220
        String recv = input.readLine(); //read 220 gov.uk
        String r[] = recv.split(" ");
        System.out.println("S:" +recv);
        if (recv.startsWith("220")) {
            //send 220 back
            serverHost = r[1];
            String reply = "HELLO " + localHost;
            System.out.println("C:"+ reply);
            output.println("220 " +reply);      //send
            //receiv 250
            String recv2 = input.readLine();  //read
            if(recv2.startsWith("250")){
                System.out.println("S:" + recv2);
                // connection establish

                is_connected = true;

                return true;
            }
        }
        return false;
    }

    /**
     *  encapsulated scripts of receiving message from the server
     * @return
     * @throws IOException
     */
    public static String reci() throws IOException {
        String response = input.readLine();
        return response;
    }


    /**
     *  main function of client
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {


        Client client = new Client("127.0.0.1", 25);


        if (args.length > 0){
            System.out.println("host: "+args[0]);
            System.out.println("port: "+args[1]);
            System.out.println();
        client = new Client(args[0], Integer.parseInt(args[1]));

        }

        if (three_hand_shake(input) == true){// true == done

            String in = "";
            while (in!="Q"){
                System.out.print("> ");
                in = Key.readLine();
                if(in.startsWith("HELLO"))
                    send(250, in);
                else if(in.startsWith("MAIL FROM:") || in.startsWith("MAIL FROM")){
                    send (250, in);
                }
                else if(in.startsWith("RCPT TO:") || in.startsWith("RECT TO") || in.startsWith("MAIL TO") || in.startsWith("MAIL TO:")){
                    send (250, in);
                }
                else if(in.startsWith("DATA") || in.equalsIgnoreCase("DATA")){
                    send (250, in);
                    System.out.println(in);

                    String income = input.readLine();
                    System.out.println("S:"+income);

                    if (income.startsWith("354")){
                        System.out.println("Email Content > ");
                        in = Key.readLine();
                        send(250, in);
                    }else{
                        System.err.println("Error::354");
                    }
                }else if (in.startsWith("QUIT")){
                    send(221, "QUIT");
                    String end = input.readLine(); // receive
                    listen(end);
                    System.out.println("S:"+end);
                    in = "Q";
                    break;
                }else{
                    System.err.println("Command not support");
                    send(250, ""); // send a empty package
                    System.out.print("> ");
                }


                // message from server side
                System.out.println(reci());

            }




            //if (command.equals("QUIT")){
            // exit connection



        }
        s.close();
        System.exit(0);
    }
}

